<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Desafio +Lev</title>

        <!-- CSS, JS e complementos -->
        <?php include ( '_inc/head.php' );?>

    </head>

    <body>

        <!-- start:: #page-content -->
        <div id="page-content">

        </div>
        <!-- end:: #page-content -->


        <!-- start:: footer -->
        <footer id="sticky-footer">

        </footer><!-- end:: footer -->
        
    </body>
</html>